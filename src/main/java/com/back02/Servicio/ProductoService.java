package com.back02.Servicio;


import com.back02.Servicio.repository.ProductoRepository;
import com.back02.modelo.ProductoModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoService {


    @Autowired
    ProductoRepository productoRepository;

    public List<ProductoModel> findAll() {
        return productoRepository.findAll();
    }

    public Optional<ProductoModel> findById(String  id) {
        return productoRepository.findById(id);
    }

    public ProductoModel save(ProductoModel entity) {
        return productoRepository.save(entity);
    }

    public boolean delete(ProductoModel entity) {
        try {
            productoRepository.delete(entity);
            return true;
        } catch(Exception ex) {
            return false;
        }
    }
}


